import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:timeago/timeago.dart' as timeago;

const primaryTextColor = Colors.black;
const primaryTextColorLight = Colors.grey;
const textColorInvert = Colors.white;
// const accentColor = Color.fromRGBO(105, 218, 219, 1);
const backgroundColor = Colors.white;

const imageTagSuffix = "-image";
const titleTagSuffix = "-title";

MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  Map<int, Color> swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  for (var strength in strengths) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  }
  return MaterialColor(color.value, swatch);
}

Widget buildPageStatus(String imageName, String statusText) {
  return Center(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset("assets/images/" + imageName, width: 150),
        Text(
          statusText,
          textAlign: TextAlign.center,
        )
      ],
    ),
  );
}

InputDecoration formInputDecoration(String hintText, {IconData? icon}) {
  return InputDecoration(
      contentPadding: const EdgeInsets.symmetric(vertical: 2, horizontal: 10),
      hintText: hintText,
      errorMaxLines: 5,
      filled: true,
      fillColor: Colors.grey.shade400.withOpacity(.2),
      // fillColor: const Color.fromRGBO(245, 245, 245, 1),
      prefixIcon: icon != null
          ? Icon(
              icon,
              color: primaryTextColorLight,
            )
          : null,
      border: const OutlineInputBorder(
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.all(Radius.circular(15))));
}

class NoGlowScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

String getTimeAgoFromDate(DateTime theDate) {
  timeago.setLocaleMessages('fr', timeago.FrMessages());

  return timeago.format(theDate, locale: 'fr');
}
