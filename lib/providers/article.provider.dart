import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:newsapp/models/article.model.dart';
import 'package:newsapp/models/category.model.dart';

class ArticleProvider with ChangeNotifier {
  late List<Article> _articles;
  late List<Category> _articleCategories;
  late Category? _activeCategory;
  String _searchTerm = "";

  List<Article> get articleToDisplay => _articles.where((element) {
        if (_searchTerm.isEmpty) return element.category == _activeCategory;
        return element.category == _activeCategory &&
            element.title.toLowerCase().contains(_searchTerm.toLowerCase());
      }).toList();

  List<Category> get categories => _articleCategories;

  Future<void> loadData() async {
    try {
      final dataFileContent = await rootBundle.loadString("assets/data.json");

      final jsonContent = jsonDecode(dataFileContent);

      _articleCategories = (jsonContent["category"] as List<dynamic>).map((e) {
        return Category.fromMap(e);
      }).toList();

      _activeCategory = _articleCategories.first;

      _articles = (jsonContent["articles"] as List<dynamic>).map((e) {
        // print("++++article e => $e");
        final thisArticleCategory = _articleCategories.firstWhere(
          (category) => category.id == e["categoryId"],
          orElse: () => throw "Unknow category with id ${e['categoryId']}",
        );
        return Article.fromMap(e, thisArticleCategory);
      }).toList();
    } catch (e, stackTrace) {
      print(stackTrace);
    }
  }

  bool isActiveCategory(Category theCategory) {
    if (_activeCategory == null) return false;
    return _activeCategory == theCategory;
  }

  void setSearchTerm(String term) {
    _searchTerm = term;
    notifyListeners();
  }

  void setActiveCategory(Category theCategory) {
    _activeCategory = theCategory;
    notifyListeners();
  }
}
