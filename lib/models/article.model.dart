import 'dart:convert';

import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/models/category.model.dart';

class Article {
  final String id;
  final String title;
  final String coverPhotoUrl;
  final String content;
  final DateTime publicationDateTime;
  final String author;
  Category category;

  String get publicationTimeAgo => getTimeAgoFromDate(publicationDateTime);

  Article({
    required this.id,
    required this.title,
    required this.coverPhotoUrl,
    required this.content,
    required this.publicationDateTime,
    required this.author,
    required this.category,
  });

  Article copyWith({
    String? id,
    String? title,
    String? coverPhotoUrl,
    String? content,
    DateTime? publicationDateTime,
    String? author,
    int? viewCounter,
    Category? category,
  }) {
    return Article(
      id: id ?? this.id,
      title: title ?? this.title,
      coverPhotoUrl: coverPhotoUrl ?? this.coverPhotoUrl,
      content: content ?? this.content,
      publicationDateTime: publicationDateTime ?? this.publicationDateTime,
      author: author ?? this.author,
      category: category ?? this.category,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'coverPhotoUrl': coverPhotoUrl,
      'content': content,
      'publicationDateTime': publicationDateTime.millisecondsSinceEpoch,
      'author': author,
      'category': category.toMap(),
    };
  }

  factory Article.fromMap(Map<String, dynamic> map, Category category) {
    return Article(
        id: map['id'],
        title: map['title'],
        coverPhotoUrl: map['coverPhotoUrl'],
        content: map['content'],
        publicationDateTime:
            DateTime.fromMillisecondsSinceEpoch(map['publicationDateTime']),
        author: map['author'],
        category: category);
  }

  @override
  String toString() {
    return 'Article(id: $id, title: $title, coverPhotoUrl: $coverPhotoUrl, content: $content, publicationDateTime: $publicationDateTime, author: $author, category: $category)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Article &&
        other.id == id &&
        other.title == title &&
        other.coverPhotoUrl == coverPhotoUrl &&
        other.content == content &&
        other.publicationDateTime == publicationDateTime &&
        other.author == author &&
        other.category == category;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        title.hashCode ^
        coverPhotoUrl.hashCode ^
        content.hashCode ^
        publicationDateTime.hashCode ^
        author.hashCode ^
        category.hashCode;
  }
}
