import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/models/article.model.dart';
import 'package:newsapp/widget/article_viewer_content.dart';

const double imageDisplayerHeight = 600;

class ArticleViewer extends StatelessWidget {
  static const routeName = "/article-viewer";

  const ArticleViewer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theArticle = ModalRoute.of(context)!.settings.arguments as Article;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          _ArticleViewerImageDisplayer(
            imageUrl: theArticle.coverPhotoUrl,
            imageTag: theArticle.id + imageTagSuffix,
          ),
          ArticleViewerContent(
            imageDisplayerHeight: imageDisplayerHeight,
            theArticle: theArticle,
          )
        ],
      ),
    );
  }
}

class _ArticleViewerImageDisplayer extends StatelessWidget {
  final String imageUrl;
  final String imageTag;

  const _ArticleViewerImageDisplayer(
      {Key? key, required this.imageUrl, required this.imageTag})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      imageBuilder: (context, imageProvider) => Stack(
        children: [
          Hero(
            tag: imageTag,
            child: Container(
              height: imageDisplayerHeight,
              decoration: BoxDecoration(
                color: Colors.white,
                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              ),
            ),
          ),
          Container(
            height: imageDisplayerHeight,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.transparent, Colors.black]),
            ),
          )
        ],
      ),
      placeholder: (context, url) => const CircularProgressIndicator(
        color: Colors.grey,
      ),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }
}
