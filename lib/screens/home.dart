import 'package:flutter/material.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/providers/article.provider.dart';
import 'package:newsapp/widget/article_list.dart';
import 'package:newsapp/widget/category_tab_bar.dart';
import 'package:newsapp/widget/search_form.dart';
import 'package:provider/provider.dart';

class Home extends StatelessWidget {
  static const routeName = "/";

  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: false,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  SizedBox(
                    width: 100,
                    height: 50,
                    child: Stack(
                      children: [
                        Positioned(
                          left: -13,
                          child: IconButton(
                              onPressed: () => null,
                              icon: const Icon(Icons.menu)),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Text(
                    "Bienvenu(e)",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                  Text(
                    "Le meilleur de l'actualité à portée de main",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  const SearchForm(),
                ],
              ),
            ),
            const SizedBox(
              height: 30,
            ),
            const HomeDynamicContent()
          ],
        ),
      ),
    );
  }
}

class HomeDynamicContent extends StatefulWidget {
  const HomeDynamicContent({Key? key}) : super(key: key);

  @override
  _HomeDynamicContentState createState() => _HomeDynamicContentState();
}

class _HomeDynamicContentState extends State<HomeDynamicContent> {
  late ArticleProvider _articleProvider;
  bool _isLoading = false;
  bool _initialDataLoadingCompleted = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _articleProvider = Provider.of<ArticleProvider>(context, listen: true);
    _loadData();
  }

  @override
  Widget build(BuildContext context) {
    if (_isLoading) return _loadingScreen();

    return Expanded(
      child: Column(
        children: const [
          Padding(
            padding: EdgeInsets.only(left: 20),
            child: CategoryTabBar(),
          ),
          SizedBox(height: 10),
          ArticleList()
        ],
      ),
    );
  }

  Future<void> _loadData() async {
    if (_initialDataLoadingCompleted) return;
    setState(() {
      _isLoading = true;
    });
    await _articleProvider.loadData();
    setState(() {
      _initialDataLoadingCompleted = true;
      _isLoading = false;
    });
  }

  Widget _loadingScreen() {
    return Column(
      children: const [ListTileShimmer(), ListTileShimmer(), ListTileShimmer()],
    );
  }
}
