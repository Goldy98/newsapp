import 'package:auto_animated/auto_animated.dart';
import 'package:flutter/material.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/providers/article.provider.dart';
import 'package:newsapp/widget/article_list_item.dart';
import 'package:provider/provider.dart';

class ArticleList extends StatelessWidget {
  const ArticleList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.only(left: 20.0),
        child: ScrollConfiguration(
          behavior: NoGlowScrollBehavior(),
          child: Consumer<ArticleProvider>(
              builder: (context, articleProvider, child) {
            if (articleProvider.articleToDisplay.isEmpty) {
              return buildPageStatus(
                  "no-data.svg", "Aucun article ne réponds à ces critères");
            }
            return LiveList(
                delay: const Duration(milliseconds: 10),
                showItemInterval: const Duration(milliseconds: 150),
                itemBuilder: (context, index, Animation<double> animation) =>
                    FadeTransition(
                      opacity: Tween<double>(
                        begin: 0,
                        end: 1,
                      ).animate(animation),
                      child: SlideTransition(
                        position: Tween<Offset>(
                          begin: const Offset(0, -0.1),
                          end: Offset.zero,
                        ).animate(animation),
                        child: ArticleListItem(
                            article: articleProvider.articleToDisplay[index]),
                      ),
                    ),
                itemCount: articleProvider.articleToDisplay.length);
          }),
        ),
      ),
    );
  }
}
