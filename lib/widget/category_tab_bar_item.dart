import 'package:flutter/material.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/models/category.model.dart';

class CategoryTabBarItem extends StatelessWidget {
  final Category theCategory;
  final bool isActiveCategory;
  final VoidCallback onClick;

  const CategoryTabBarItem(
      {Key? key,
      required this.theCategory,
      this.isActiveCategory = false,
      required this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onClick,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        child: Text(
          theCategory.name,
          style: TextStyle(
              fontWeight: FontWeight.w800,
              color: isActiveCategory
                  ? primaryTextColor.withOpacity(.8)
                  : primaryTextColorLight,
              fontSize: 20),
        ),
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: isActiveCategory
                        ? primaryTextColor.withOpacity(.8)
                        : primaryTextColorLight,
                    width: isActiveCategory ? 3 : 1.5))),
      ),
    );
  }
}
