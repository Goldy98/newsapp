import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/models/article.model.dart';

class ArticleViewerContent extends StatelessWidget {
  final double imageDisplayerHeight;
  final Article theArticle;

  const ArticleViewerContent(
      {Key? key, required this.imageDisplayerHeight, required this.theArticle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: ScrollConfiguration(
        behavior: NoGlowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    IconButton(
                        onPressed: () => Navigator.pop(context),
                        icon: const Icon(
                          Icons.arrow_back_ios,
                          color: textColorInvert,
                        )),
                    SizedBox(height: imageDisplayerHeight - 300),
                    Row(
                      children: [
                        Container(
                            child: Text(
                              theArticle.category.name,
                              style: const TextStyle(color: textColorInvert),
                            ),
                            padding: const EdgeInsets.all(7),
                            decoration: BoxDecoration(
                                color: Colors.white.withOpacity(.2),
                                borderRadius: BorderRadius.circular(20))),
                        const SizedBox(
                          width: 10,
                        ),
                        Container(
                          decoration: const BoxDecoration(
                              color: Colors.black,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(100))),
                          padding: const EdgeInsets.all(8),
                          child: Row(
                            children: [
                              const Icon(
                                Icons.person_outline_rounded,
                                color: Colors.white,
                              ),
                              const SizedBox(
                                width: 3,
                              ),
                              Text(theArticle.author,
                                  style: const TextStyle(
                                    color: textColorInvert,
                                  )),
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Text(
                      theArticle.title,
                      style: const TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 23,
                          color: textColorInvert),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                width: double.infinity,
                padding: const EdgeInsets.all(20),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(40),
                        topRight: Radius.circular(40))),
                child: Html(
                  data: theArticle.content,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
