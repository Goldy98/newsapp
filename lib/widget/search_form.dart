import 'package:flutter/material.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/providers/article.provider.dart';
import 'package:provider/provider.dart';

class SearchForm extends StatelessWidget {
  const SearchForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: primaryTextColorLight,
      onChanged: (newValue) =>
          Provider.of<ArticleProvider>(context, listen: false)
              .setSearchTerm(newValue),
      decoration:
          formInputDecoration("Rechercher un article", icon: Icons.search),
    );
  }
}
