import 'package:flutter/material.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/models/category.model.dart';
import 'package:newsapp/providers/article.provider.dart';
import 'package:newsapp/widget/category_tab_bar_item.dart';
import 'package:provider/provider.dart';

class CategoryTabBar extends StatelessWidget {
  const CategoryTabBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      child: ScrollConfiguration(
        behavior: NoGlowScrollBehavior(),
        child: Consumer<ArticleProvider>(
            builder: (context, articleProvider, child) {
          return ListView(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            children: articleProvider.categories
                .map((cat) => CategoryTabBarItem(
                    theCategory: cat,
                    isActiveCategory: articleProvider.isActiveCategory(cat),
                    onClick: () => articleProvider.setActiveCategory(cat)))
                .toList(),
          );
        }),
      ),
    );
  }
}
