import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:newsapp/helpers/ui.dart';
import 'package:newsapp/providers/article.provider.dart';
import 'package:newsapp/screens/article_viewer.dart';
import 'package:newsapp/screens/home.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

void main() async {
  runApp(ChangeNotifierProvider(
      create: (context) => ArticleProvider(), child: const MyApp()));
  await SystemChrome?.setPreferredOrientations([DeviceOrientation.portraitUp]);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: "OpenSans",
        textTheme: const TextTheme(
          headline1: TextStyle(
              color: Colors.black,
              fontSize: 40.0,
              fontWeight: FontWeight.w900,
              fontFamily: "Quicksand"),
          headline5: TextStyle(fontSize: 24),
          headline6: TextStyle(fontSize: 20.0),
          bodyText2: TextStyle(fontSize: 14.0),
          bodyText1: TextStyle(fontSize: 14.0, color: primaryTextColorLight),
        ),
      ),
      home: const Home(),
      onGenerateRoute: (settings) {
        switch (settings.name) {
          case ArticleViewer.routeName:
            return PageTransition(
                child: const ArticleViewer(),
                type: PageTransitionType.bottomToTop,
                settings: settings);
          default:
            return null;
        }
      },
    );
  }
}
