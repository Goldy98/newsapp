# NewsApp

Test app made for my application for **Flutter App Developper** role at [**EverPartner**](https://ever.partners/)

## How to run the project

### Install dependancies

```shell
pub get
```

### Launch the app

```shell
flutter run --no-sound-null-safety
```

## App Screenshots

|                                                       Splashscreen                                                       |                                                     Article Listing with item                                                      |                                                        Article Reader                                                        |
| :----------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------------: | :--------------------------------------------------------------------------------------------------------------------------: |
| <img src="https://gitlab.com/Goldy98/newsapp/-/raw/main/screenshots/splashscreen.jpg" title="Splashscreen" width="100%"> | <img src="https://gitlab.com/Goldy98/newsapp/-/raw/main/screenshots/home-full.jpg" title="Article Listing with item" width="100%"> | <img src="https://gitlab.com/Goldy98/newsapp/-/raw/main/screenshots/article-reader.jpg" title="Article Reader" width="100%"> |
